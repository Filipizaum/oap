Este DVD contém uma página HTML com uma aula a ser executada em um navegador.

Para executar em Windows:
- Abra "Meu Computador";
- Clique duas vezes no Driver de CD/DVD no qual está inserido;
- Clique em "Executar aprendizado.bat"
- Uma página vai abrir no navegador padrão.
- Caso não queira que abra neste navegador, siga as instruções de "Para executar em outros sistemas operacionais".

Para executar em outros sistemas operacionais:
- Abra o driver de CD/DVD no qual está inserido;
- Clique com o botão direito no arquivo "index.html";
- Escolha a oção "Abrir com..."  e o navegador de sua preferência.

Tecnologias utilizadas

 --- CSS ---
Twitter Bootstrap v3.1.1
http://getbootstrap.com/

 --- Java Script --- 
jQuery v1.10.2
http://jquery.com/

qTip2 v2.2.1
http://craigsworks.com/projects/qtip/

jQuery maphilight
http://davidlynch.org/projects/maphilight/docs/

 --- Imagens --- 
book.ico
http://findicons.com/icon/254755/contents?id=256931

aprendizado.ico
https://www.iconfinder.com/icons/104617/book_learning_icon#size=128